﻿##import os
##os.environ["THEANO_FLAGS"] = "mode=FAST_RUN,device=gpu,floatX=float32"

import pyganim
import pygame, sys
from pygame import *
from pygame.locals import *
from sklearn import preprocessing
from sklearn.linear_model import Perceptron
import keras
import numpy as np
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation
from keras.optimizers import SGD
from sklearn.externals import joblib


def
DISPLAYSURF = pygame.display.set_mode((1600,100), 0, 32)   # настройка окна
pygame.display.set_caption('Drawing')

BLACK = (  30,  30,   30)    # определение цветов
WHITE = (255, 255, 255)
RED   = (255,   0,   0)
GREEN = (  0, 255,   0)
BLUE  = (  0,   0, 255)

DISPLAYSURF.fill(WHITE)    # рисование
##pygame.draw.polygon(DISPLAYSURF, GREEN, ((146, 0), (291, 106), (236, 277),   (56, 277), (0, 106)))
width = DISPLAYSURF.get_width()
height = DISPLAYSURF.get_height()
a=(width/2)
b=(height/2)
LearnRate=0.9#may depend on t///
Xc=a
SELLSIZE=0.3
YVELMAX=7.0
YVELMIN=-7.0
xVELMIN=-7.0
xVELMAX=7.0
WIDTH=5
DELAY=0
HEIGHT=15
g=0
DRAWING=True
NUMPLAYERS=4
NUMSURVIVE=1
NUMDIE=1
RewardSize=100
ActivationNumbers ={0:'softmax',1:'softplus',2:'relu',3:'tanh',
                    4:'sigmoid',5:'hard_sigmoid',6:'linear'}
DiscountFactor = 0.9 #wheter model will car bout further or current reward >=further
def my_loss(Xc,Xk,Vk,V,n):
    return abs((Xk+Vk+V*(-1)**n)-Xc)

##def Qfunc(state,action,statenext,t):
##    if t==1:
##        return 10
##    return Qfunc(state,action,statenext,t-1)+LearnRate*((RewardSize-my_loss(Xc,state[0][0],state[0][1],SELLSIZE,action))+DiscountFactor*max([Qfunc(statenext,action,statenext,t-1)])-Qfunc(state,action,statenext,t-1))

##def my_loss(Xc,Xk,Vk,V,n):
##    return abs((Xk+Vk+V*(-1)**n)-Xc)
##    
#clf=SGDClassifier(penalty="l2")
model = Sequential()
model.add(Dense(3, input_dim=2, init='uniform', activation='relu'))
#model.add(Dropout(0.5))
model.add(Dense(2,activation='relu'))
#model.add(Dropout(0.5))
model.add(Dense(1, activation='sigmoid'))

model.compile(loss='binary_crossentropy',
              optimizer='rmsprop',
              class_mode='binary')

def GenerateModel(maxLayers,maxOut,inputDim):
    model = Sequential()
    #numLayers=np.random.randint(maxLayers)
    #numOut=np.random.randint(maxOut)
    numLayers=maxLayers
    numOut=maxOut
    numActivation = np.random.randint(7)
    model.add(Dense(numOut,input_dim=inputDim,init='uniform',
                        activation=ActivationNumbers[numActivation]))
    for i in xrange(0,numLayers):
        #numOut=np.random.randint(maxOut)
        numActivation = np.random.randint(7)
        model.add(Dense(numOut,
                        activation=ActivationNumbers[numActivation]))
    model.add(Dense(1, activation='sigmoid'))
    model.compile(loss='binary_crossentropy',
              optimizer='rmsprop',
              class_mode='binary')
    return model

def Mutate(model):
    wer=model.get_weights()
    numLayers=len(model.layers)
    for i in xrange(0,numLayers*2,2):
        for row in range(0,len(wer[i])):
            for elem in range(0,len(wer[i][row])):
                if np.random.randint(10)==1:
                   # print 'mutateW'
                    wer[i][row][elem]=2*np.random.rand()-1
    for i in xrange(1,numLayers*2,2):
        for elem in range(0,len(wer[i])):
            if np.random.randint(3)==2:
                #print 'mutateB'
                wer[i][elem]=2*np.random.rand()-1
    model.set_weights(wer)
    return model


def Recombinate(model1,model2):
    wer1=model1.get_weights()
    wer2=model2.get_weights()
    numLayers=len(model1.layers)
    for i in xrange(0,numLayers*2,2):
        for row in range(0,len(wer1[i])):
            for elem in range(0,len(wer1[i][row])):
                if np.random.randint(3)==2:
                    wer1[i][row][elem],wer2[i][row][elem]=wer2[i][row][elem],wer1[i][row][elem]
    for i in xrange(1,numLayers*2,2):
        for elem in range(0,len(wer1[i])):
            if np.random.randint(3)==2:
                wer1[i][elem],wer2[i][elem]=wer2[i][elem],wer1[i][elem]
    model1.set_weights(wer1)
    model2.set_weights(wer2)
    return  model1,model2
pygame.init()


class Driver():
    def __init__(self):
        self.model=Sequential()
        self.Score=0
        self.desision=[[0]]
    def SaveModel(self,name):
        joblib.dump(self.model.get_weights(),'model'+name)
        


class Player(sprite.Sprite):
    def __init__(self, x, y):
        sprite.Sprite.__init__(self)
        self.xvel = 0   #скорость перемещения. 0 - стоять на месте
        self.yvel = 0
        self.startX = x # Начальная позиция Х, пригодится когда будем переигрывать уровень
        self.startY = y
        self.image = Surface((WIDTH,HEIGHT))
        self.color=BLACK
        self.image.fill(self.color)
##        self.image1 = Surface((WIDTH,HEIGHT))
##        self.image1.fill(BLACK)
##        self.image2 = Surface((WIDTH,HEIGHT))
##        self.image2.fill(RED)
        self.rect = Rect(x, y, WIDTH, HEIGHT) # прямоугольный объект
        self.Driver=Driver()
        self.left=False
        self.right=False
##        self.leftedge = (self.startX,self.startY)
##        self.rightedge=self.leftedge
        #AnimQueue=[]
        #delay=10
        #AnimQueue.append((self.image1,delay))
        #AnimQueue.append((self.image2,delay))
        #self.ActiveAnim=pyganim.PygAnimation(AnimQueue)
        #self.ActiveAnim.play()
    def ChangeColor(self,color):
        self.image.fill(color)
        self.color=color
    def update(self):
        
        if self.left:
            if(self.xvel>=xVELMIN+SELLSIZE):
               # print("left")
                self.xvel -= SELLSIZE # Лево = x- n
            #self.leftedge = (self.rect.x,self.rect.y)#most~ distant pixel
            #self.image.fill(BLACK)
        if self.right:
            if(self.xvel<=xVELMAX-SELLSIZE):
                #print("RIGHT")
                self.xvel += SELLSIZE # Право = x + n

        self.rect.x += self.xvel*1
            #self.rightedge = (self.rect.x,self.rect.y)
            #self.image.fill(BLACK)
##        if up:
##            if(self.yvel>YVELMIN):
##                self.yvel -= SELLSIZE # Лево = x- n
##            self.image.fill(BLACK)
##        if down:
##            if(self.yvel<YVELMAX):
##                self.yvel += SELLSIZE # Право = x + n
##            self.image.fill(BLACK)
##        if goleft:
##            self.rect.x,self.rect.y=self.leftedge
            #elf.rect.y=self.leftedge
##        if not(left or right): # стоим, когда нет указаний идти
##            self.xvel = 0
        #self.rect.y+=self.yvel*1+(g*(0.01**2)/2)
##        if(self.yvel<YVELMAX):
##            self.yvel+=g*0.01
        
        #self.xvel+=g*0.1
##        self.rect.x += self.xvel # переносим свои положение на xvel 
        #if not(left or right or up or down):
            #self.ActiveAnim.blit(self.image, (0,0))
        
    def draw(self, DISPLAYSURF): # Выводим себя на экран
        DISPLAYSURF.blit(self.image, (self.rect.x,self.rect.y))


a=(width/2)
b=(height/2)


#crack = Player(a+50,b)
left= False
right = False
down= False
up = False
goleft=False
Qfuncprev=10
t=0
Players=[]
for i in xrange(0,NUMPLAYERS):
    newcrack=Player(a+50,b)
    newcrack.Driver.model=GenerateModel(1,3,2)
    if DRAWING:
        if NUMPLAYERS<16:
            newcrack.ChangeColor((i,i+i**2,(i**2)-i))
        else:
            print 'I dont wanna to create so much colors'
    Players.append(newcrack)
Players[1].ChangeColor(BLUE)
Players[2].ChangeColor(GREEN)
#Driv=Driver()
def SortByScore(Player):
    return Player.Driver.Score
#Driv.model=GenerateModel(2,3,2)
epochcounter=0




w.show()
while True:      # основной цикл обработки событий
    #desision=clf.predict(crack.rect.x)
   # Show the window and run the app
    
#app.exec_()
    t+=1
    
   
    for driv in Players:
        x=np.array([driv.rect.x,driv.xvel]).reshape((1,2))
        driv.Driver.desision=driv.Driver.model.predict_classes(x,verbose=0)
    #prev=my_loss(a,crack.rect.x,crack.xvel,SELLSIZE,desision[0][0])
    DISPLAYSURF.fill(WHITE)
    for event in pygame.event.get():
##        if event.type == KEYDOWN:
##            if event.key == pygame.K_LEFT:
####                left=True
##                print 'Set Drawing(0/1) '
##                enable=int(input())
##                if enable==1:
##                    DRAWING=True
##                else:
##                    DRAWING=False
##            if event.key == pygame.K_UP:
##                print 'set NUMSURVIVE'
##                NUMSURVIVE=int(input())
######                up=True
##            if event.key == pygame.K_DOWN:
##                print 'Set DELAY'
##                DELAY=int(input())
####                down=True   
##            if event.key == pygame.K_RIGHT:
##                right=True
##        if event.type == KEYUP:
##            if event.key == pygame.K_LEFT:
##                left=False
####            if event.key == pygame.K_UP:
####                up=False
####            if event.key == pygame.K_DOWN:
####                down=False   
##            if event.key == pygame.K_RIGHT:
##                right=False
        if event.type == QUIT:
            print 'wat model to save?'
            var=int(input())
            if var>=0:
                print 'name'
                Players[var].Driver.SaveModel(input())
            pygame.quit()
            sys.exit()
    for driv in Players:
        if(driv.Driver.desision[0][0]==1):
            driv.left=True
        else:
            driv.right=True
        if DRAWING==True:
            driv.update()
            driv.draw(DISPLAYSURF)
    pygame.draw.rect(DISPLAYSURF, RED, (a,b,5, 15))
    #crack.update(left, right) # передвижение
    #crack.draw(DISPLAYSURF) # отображение
    pygame.time.wait(DELAY)
    w.updateGeometry()
    if DRAWING:
        pygame.display.update()
    
    #left=False
    #right=False
##    if(my_loss(a,crack.rect.x,crack.xvel,SELLSIZE,desision[0][0])-prev >0):
##        model.train_on_batch(x,[abs(desision[0][0]-1)])
    #Qf=Qfunc(x,desision[0][0],np.array([crack.rect.x,crack.xvel]).reshape((1,2)),t)
##    if (Qf>=Qfuncprev):
##        Qfuncprev=Qf
##    else:
##        Qfuncprev=Qf
##        model.train_on_batch(x,[abs(desision[0][0]-1)])
   
##        else:
##            driv.Driver.Score-=1
    for driv in Players:
        #if (abs(Xc-driv.rect.x)<20) :
        driv.Driver.Score+=(abs(Xc-driv.rect.x))  
        driv.left=False
        driv.right=False
    if t==350:
        
        #for driv in Players:
                
##            if driv.Driver.Score<-100000:
##                driv.Driver.model=GenerateModel(2,4,2)
##                print 'killed'
                
        Players.sort(key=SortByScore)
        for i in xrange(NUMPLAYERS-1,NUMDIE,-1):
            Players[i].Driver.model=GenerateModel(1,3,2)
            
        tobreed1=np.random.randint(NUMSURVIVE)
        tobreed2=np.random.randint(NUMSURVIVE)
        Players[NUMPLAYERS-1].Driver.model,Players[NUMPLAYERS-2].Driver.model=Recombinate(Players[tobreed1].Driver.model,Players[tobreed2].Driver.model)
        Players[NUMPLAYERS-3].Driver.model,Players[NUMPLAYERS-4].Driver.model=Recombinate(Players[0].Driver.model,Players[1].Driver.model)
        Players[NUMPLAYERS-5].Driver.model=Mutate(Mutate(Players[0].Driver.model))
        Players[NUMPLAYERS-6].Driver.model=Mutate(Mutate(Mutate(Players[0].Driver.model)))
        for i in xrange(NUMSURVIVE,NUMPLAYERS-NUMDIE):
            Players[i].Driver.model=Mutate(Players[i-1].Driver.model)
        
        t=0
        if np.random.randint(100)==1:
            Players[1].Driver.model=Mutate(Players[1].Driver.model)
        print 'Best Score', Players[0].Driver.Score
        epochcounter+=1
        
        for driv in Players:
            driv.rect.x=np.random.randint(500)
            driv.xvel=0
            driv.Driver.Score=0
        print 'New week(',epochcounter,'): number of dumbass players increases'
        
    #print 'imher',crack.rect.x
